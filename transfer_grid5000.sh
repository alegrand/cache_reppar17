#!/bin/bash
# Script for copying ParaK to GRID5000 access machine

echo "Copying ParaK to grid5000 machine"
mkdir Empty
scp -r Empty lustanisic@access.grenoble.grid5000.fr:ParaK
scp -r Empty lustanisic@access.grenoble.grid5000.fr:ParaK/data
scp -r Empty lustanisic@access.grenoble.grid5000.fr:ParaK/src
scp -r Empty lustanisic@access.grenoble.grid5000.fr:ParaK/src/inputGenerator

scp src/program.c lustanisic@access.grenoble.grid5000.fr:ParaK/src
scp src/Makefile lustanisic@access.grenoble.grid5000.fr:ParaK/src
scp src/inputGenerator/InputGenerator.c lustanisic@access.grenoble.grid5000.fr:ParaK/src/inputGenerator
scp src/inputGenerator/Makefile lustanisic@access.grenoble.grid5000.fr:ParaK/src/inputGenerator

scp run.sh lustanisic@access.grenoble.grid5000.fr:ParaK
scp Parameters.txt lustanisic@access.grenoble.grid5000.fr:ParaK
scp all_run.sh lustanisic@access.grenoble.grid5000.fr:ParaK
scp producing_pdf.sh lustanisic@access.grenoble.grid5000.fr:ParaK
scp setup* lustanisic@access.grenoble.grid5000.fr:ParaK
rmdir Empty

ssh lustanisic@access.grenoble.grid5000.fr
