#!/bin/bash
# Script for running multiple instances of kernel

set -e # fail fast

program_name="src/program_Intel"
# Possibility to use command line argument
param1=$1
if [[ $# == 1 ]]; then
    program_name=$param1
fi

#Change to run.sh
echo -n "Enter number of threads: "
read mult_n
echo -n "Enter core for main thread:"
read main_core
i="0"
while [ $i -lt $mult_n ]
do
echo -n "Enter core for thread [$i]:"
read mcores[$i]
i=`expr $i + 1`
done
#End change

#Producing the original name for .dat file
bkup=0
while [ -e data/ParaKData${bkup}.dat* ] ; do 
  bkup=`expr $bkup + 1`
done
file_param="Parameters.txt"
file_input="data/ParaKInput${bkup}.dat"
file_kernel="data/ParaKKernel${bkup}.dat"
filedat="data/ParaKData${bkup}.dat"

#################################################################
#Makefile of InputGenerator.c
cd src/inputGenerator
make clean
make
cd ..
cd ..

#Executing InputGenerator.c
src/inputGenerator/InputGenerator $file_param $file_input

#Cleaning inputGenerator file
cd src/inputGenerator
make clean
cd ..
cd ..

#################################################################

##Reading kernel input parameters:
# Link filedescriptor 10 with stdin
exec 10<&0
# stdin replaced with a file supplied as a first argument
exec < $file_input
read sign gcc_new
read sign core
read sign alloctype
read sign seed
read sign allocmode
read sign static_size
read sign type_mem
read sign accesses_sqr
read sign ubuffer
read sign warmup
# restore stdin from filedescriptor 10
# and close filedescriptor 10
exec 0<&10 10<&-

# Copying the measurement lines
grep -v '!' $file_input > $file_kernel

#Makefile of kernel.c
cd src
make clean
	#Setting gcc and buffer type
	if [[ $gcc_new -eq "3" && $alloctype -eq "1" ]]; then
	 	make WARNINGS='$(NO_PRAYER_FOR_THE_WICKED)' TYPE='"int"' > make.out 2>&1		
	else	if [[ $gcc_new -eq "2" && $alloctype -eq "1" ]]; then
	 	make WARNINGS='$(REASONABLY_CAREFUL_DUDE)' TYPE='"int"' > make.out 2>&1
	else	if [[ $gcc_new -eq "3" && $alloctype -eq "2" ]]; then
	 	make WARNINGS='$(NO_PRAYER_FOR_THE_WICKED)' TYPE='"long long int"' > make.out 2>&1
	else	if [[ $gcc_new -eq "2" && $alloctype -eq "2" ]]; then
	 	make WARNINGS='$(REASONABLY_CAREFUL_DUDE)' TYPE='"long long int"' > make.out 2>&1
	else	if [[ $gcc_new -eq "3" && $alloctype -eq "3" ]]; then
	 	make WARNINGS='$(NO_PRAYER_FOR_THE_WICKED)' TYPE='"int"' VECTOR='"1"' > make.out 2>&1
	else	if [[ $gcc_new -eq "2" && $alloctype -eq "3" ]]; then
	 	make WARNINGS='$(REASONABLY_CAREFUL_DUDE)' TYPE='"int"' VECTOR='"1"' > make.out 2>&1
	else 
		make > make.out 2>&1
		fi
		fi
	        fi 
		fi
		fi
	fi
cd ..

echo "############################################" >> $filedat
date | sed 's/^/# /' >> $filedat
echo "############################################" >> $filedat
echo "# EXECUTION PRIORITY: " $2 >> $filedat
echo "FILE NAME: " $filedat | sed 's/^/# /' >> $filedat
echo "############################################" >> $filedat
echo "NUMBER OF THREADS: " $mult_n | sed 's/^/# /' >> $filedat
echo "MAIN THREAD IS RUNNING ON CORE: ${main_core}" | sed 's/^/# /' >> $filedat
j="0"
while [ $j -lt $mult_n ]
do
echo "THREAD $j IS RUNNING ON CORE: ${mcores[$j]}" | sed 's/^/# /' >> $filedat
j=`expr $j + 1`
done
echo "############################################" >> $filedat
echo "# MEMORY HIERARCHY:" >> $filedat
lstopo | sed 's/^/# /' >> $filedat
echo "############################################" >> $filedat
echo "# VERSION:" >> $filedat
cat /proc/version | sed 's/^/# /' >> $filedat
echo "############################################" >> $filedat
echo "# CPU INFO:" >> $filedat
cat /proc/cpuinfo | sed 's/^/# /' >> $filedat
echo "############################################" >> $filedat
echo "# CPU GOVERNOR:" >> $filedat
cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor | sed 's/^/# /' >> $filedat
echo "# CPU FREQUENCY:" >> $filedat
cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq | sed 's/^/# /' >> $filedat
echo "############################################" >> $filedat
#echo "# SVN REVISION:" >> $filedat
#svn info -r 'HEAD' | sed 's/^/# /' >> $filedat

#Make.out
echo "############################################" >> $filedat
echo "# COMPILATION:" >> $filedat
cat src/make.out | sed 's/^/# /' >> $filedat

#Copying parameters
echo "############################################" >> $filedat
cat $file_param | sed 's/^/# /' >> $filedat

echo -e "Input file is generated\n"

#Running MultiK 
$program_name $file_kernel $filedat $mult_n $main_core ${mcores[*]} $seed $allocmode $static_size $type_mem $accesses_sqr $ubuffer $warmup

#Cleaning kernel file
rm -fr src/make.out
rm -fr $file_kernel
rm -fr $file_input
cd src
make clean
cd ..

