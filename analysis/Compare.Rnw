%%
\documentclass{article}
\usepackage{Sweavel}
\usepackage{graphicx}
\usepackage{verbatim}
\SweaveOpts{echo=true}

\usepackage{ifthen}
%%% For loop code
\newcommand{\forloop}[5][1]%
   {%
     \setcounter{#2}{#3}%
     \ifthenelse{#4}%
       {%
         #5%
         \addtocounter{#2}{#1}%
         \forloop[#1]{#2}{\value{#2}}{#4}{#5}%
       }%
       %Else
       {%
       }%
   }%
%%% For loop end
   \usepackage{ifthen}
% Starting on new even page 
\newcommand{\newevenside}{
        \ifthenelse{\isodd{\thepage}}{\newpage}{
        \newpage
        \phantom{placeholder} % doesn't appear on page
        \thispagestyle{empty} % if want no header/footer
        \newpage
        }
}
%
             
\begin{document}

\section{ANALYSIS}

\verbatiminput{TempParameters.dat}

This is an comparatoin of of data from files ParaK.dat for only thread 0!
\newline
\newline
New loading:
\begin{verbatim}
grep '#' data#/ParaKData#.dat > analysis/TempParameters.dat
grep -v '#' data#/ParaKData#.dat | sed -e 's/  */ /g' -e 's/://g' | cut -d ' ' -f 2,3,4,8,9 > analysis/TempComp#.dat
#Old way:
grep -v '#' data#/ParaKData#.dat | sed -e 's/  */ /g' -e 's/://g' | cut -d ' ' -f 2,3,4,7,8,9 > analysis/TempComp#.dat
\end{verbatim}

<<load,echo=false>>=
#Hardcoded values (for now):
NFILES <- 2
NTHREADS <- c(2,2)
COMB <- 2
if (COMB!=0)
  {
    NCORES <- array(c(1,1,1,0), dim=c(COMB,NFILES))
    #NCORES <- array(c(6,6,6,6,6,6,2,2,6,6,7,7), dim=c(COMB,NFILES))
  }
require(ggplot2)
require(plyr)

#Average line
line_stat <- function(fun, geom="point", ...) { stat_summary(fun.y=fun, colour="black", geom=geom, size = 1.2, ...) } 

#Caption name
cap_name <- function(num,stride,threads=0){
  output <- "EXPERIMENT NUMBER = ";
  output <- paste(output,num,"; STRIDE = ",stride,sep="")
  if(threads!=0)
    {
      thread_t <- ";\n NUMBER OF THREADS = ";
      thread_t <- paste(thread_t,threads,sep="")
      if (COMB!=0)
        {
          thread_t <- paste(thread_t," (cores: ",sep="")
          for(i in 1:COMB)
            {
              thread_t <- paste(thread_t,NCORES[i,num]," ",sep="")
            }
          thread_t <- paste(thread_t,")",sep="")
        }
      output <- paste(output,thread_t,sep="")
    }
  return(output)
}

#Execution priority
con <- file("TempParameters.dat", "rt")
line <- scan(con,"",20)
prmode <- line[13]
prnum <- line[14]
prmode <- if(prmode=="normal") "NORMAL" else
          if(prmode=="chrt")   "CHRT" else
          if(prmode=="nice")   "NICE" else "/"

#Boxplot from presentation
general_boxplot <- function(df1, L1cache=32, # most L1 cache are 32K
                            field="BANDWIDTH", field_label="BANDWIDTH (MB/s)",
                            output="", width=430,height=500, 
                            plot_boxplot=TRUE, plot_points=FALSE,
                            plot_mean=TRUE, plot_max=FALSE, origin=FALSE
                            )
{
  p <- ggplot(data = df1) + aes_string(x="SIZE", y=field) +
                            aes(colour = factor(STRIDE)) #+
                          #coord_cartesian(ylim = c(0, max(df1[[field]]) * 1.2))
  p <- p + theme_bw() + scale_colour_hue(h=c(270, 360)) 
  # + scale_colour_brewer(type='div',palette="PuBuGn")
  # + scale_colour_gradient(limits=c(0, 64), 
  #                                            breaks=unique(df1$STRIDE),
  #                                            low="blue", 
  #                                            high="red", space="Lab")
  # scale_colour_manual(values=cbPalette)
  if(plot_boxplot) 
    p <- p + geom_boxplot(
             aes(group = round_any(SIZE+10000*STRIDE, 1, floor)), 
             outlier.size = 0.2) # alpha=.5
  if(plot_points)
    p <- p + geom_point(colour="red", size = 2, alpha=.3) 
  if(plot_mean)
    p <- p + stat_summary(fun.y = mean, geom="line", size=2)
  if(plot_max)
    p <- p + stat_summary(fun.y = max, geom="line", size=2)
  p <- p +
    geom_vline(xintercept = L1cache, colour="grey30", linetype="dashed", size=0.7)  +
    geom_text(aes(x2,y2,label = texthere, hjust=-.1),colour="grey30",data.frame(x2=L1cache, y2=(max(df1[[field]]) * 1.2), texthere="L1 cache"))
  p <- p + labs(shape = "STRIDE:", colour = "STRIDE:") +
           scale_y_continuous(field_label,limits=c(0, max(df1[[field]]) * 1.2)) +
           scale_x_continuous("MEMORY SIZE (KB)",breaks=c(1,max(SIZE)/5,max(SIZE)/5*2,max(SIZE)/5*3,max(SIZE)/5*4,max(SIZE)))
  if(max(SIZE)==92)
    p <- p + scale_x_continuous("MEMORY SIZE (KB)",breaks=c(1,32))
  p <- p + opts(legend.title = theme_text(size=16, face="bold",hjust = -0.05),
                legend.text = theme_text(size = 16))
  
  if(origin)
    p <- p + facet_wrap(~ORIGIN,ncol=2)
  if(output=="") {
    print(p)
  } else {
    png(output,width,height)
    print(p)
    garbage <- dev.off()
  }
}

#Plot num
plot_file <- function(dataf,num,stride,box=FALSE,threads=0,output="",width=430,height=500) {
  
if(box)
    p <- ggplot(data = dataf[STRIDE==stride & ORIGIN==num,], aes(SIZE, BANDWIDTH, group = round_any(SIZE, 1, floor))) + geom_boxplot(colour = "blue", outlier.size = 0)
else
    p <- ggplot(data = dataf[STRIDE==stride & ORIGIN==num,], aes(SIZE, BANDWIDTH)) +  geom_point(colour = "red", size = 1,shape = 2) 
  
if (cache_line>=10)p <- p +
  geom_vline(xintercept = L1cache, colour="yellow", linetype="dashed", size=0.7) + 
  geom_text(aes(x2,y2,label = texthere, hjust=-.1),colour="yellow",data.frame(x2=L1cache, y2=max(BANDWIDTH), texthere="L1 cache")) 
if ((cache_line %% 10)==2)p <- p +
  geom_vline(xintercept = L2cache, colour="yellow", linetype="dashed", size=0.7) + 
  geom_text(aes(x2,y2,label = texthere, hjust=-.1),colour="yellow",data.frame(x2=L2cache, y2=max(BANDWIDTH), texthere="L2 cache")) 
p <- p + 
     line_stat(mean, geom="line", linetype="dotted") +
     line_stat(max, geom="line", linetype="solid") +
     scale_y_continuous("BANDWIDTH (MB/s)") +
     scale_x_continuous("MEMORY SIZE (KB)") +
     opts(title = cap_name(num,stride,threads))
if (fixY) p <- p +
  scale_y_continuous("BANDWIDTH (MB/s)",limits=c(fixYMIN, fixYMAX))

  if(output=="") {
    print(p)
  } else {
    png(output,width,height)
    print(p)
    garbage <- dev.off()
  }
}

#Reading file 
read_file <- function(file,num) {
  dataf <- read.table(file, header=T)
  #Only thread 0
  dataf <- dataf[dataf$THREAD==0,]
  dataf$SIZE <- dataf$SIZE/1024
  dataf$PRIORITY <- apply(dataf,1,function(row) prmode)
  dataf$ORIGIN <- num
  return(dataf)
}

#Reading files and binding
dataf <- read_file('TempComp1.dat',1)
for(i in 2:NFILES){
  dataf <- rbind(dataf,read_file(sprintf("TempComp%d.dat",i),i))
}  

#Figure parameters
attach(dataf)
L1cache <- 32
L2cache <- 256
cache_line <- if(sum(SIZE==L1cache)!=0) 10 else 0
cache_line <- cache_line + if(sum(SIZE==L2cache)!=0) 2 else 0
fixY <- TRUE
fixYMIN <- min(BANDWIDTH)*0.9
#fixYMIN <- 0
fixYMAX <- max(BANDWIDTH)*1.1

#summary(aov(TIME~STRIDE*SIZE,data=dataf))
#summary(lm(TIME~STRIDE*SIZE,data=dataf))
@

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newevenside
%\newpage
<<label=pointsmin,include=FALSE,echo=FALSE>>=
for(i in 1:NFILES){
 plot_file(dataf,i,min(STRIDE),FALSE,output=sprintf("for%d.png",i),NTHREADS[i])
}
@

%Hardcoded values (for now):
\newcounter{NFILES}
\setcounter{NFILES}{\Sexpr{NFILES}}
\addtocounter{NFILES}{1}

\newcounter{ct}
\forloop{ct}{1}{\value{ct} < \value{NFILES}}%
  {%
    \newpage
    \begin{figure}[h]  
      \begin{center}
        \scalebox{0.8}{
          \includegraphics{for\arabic{ct}.png}  
        }
      \end{center}
    \end{figure}
  }
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
%Boxplots, stride min
<<label=pointsminB,include=FALSE,echo=FALSE>>=
for(i in 1:NFILES){
 plot_file(dataf,i,max(STRIDE),TRUE,output=sprintf("for%db.png",i),NTHREADS[i])
}
@
\newcounter{ct2}
\forloop{ct2}{1}{\value{ct2} < \value{NFILES}}%
  {%
    \newpage
    \begin{figure}[h]  
      \begin{center}
        \scalebox{0.8}{
          \includegraphics{for\arabic{ct2}b.png}  
        }
      \end{center}
    \end{figure}
  }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage
<<label=compALL,include=FALSE,echo=FALSE>>= 
general_boxplot(dataf,output='compALL.png',origin=TRUE)
@
\begin{figure}[h]  
\begin{center}  
    \includegraphics[width=\linewidth]{compALL.png}
\end{center}    
\caption{All strides, average values}
\end{figure}

\end{document}
