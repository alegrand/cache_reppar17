%%
\documentclass{article}
\usepackage{Sweavel}
\usepackage{graphicx}
\usepackage{verbatim}
\usepackage{subfig}
\SweaveOpts{echo=true}

\begin{document}

\section{ANALYSIS}

\verbatiminput{TempParameters.dat}

This is an analysis of data from file ParaK.dat of only thread 0!
\newline
\newline
New loading:
\begin{verbatim}
grep '#' data#/ParaKData#.dat > analysis/TempParameters.dat
grep -v '#' data#/ParaKData#.dat | sed -e 's/  */ /g' -e 's/://g' | cut -d ' ' -f 2,3,4,5,6,8,9,10,11 > analysis/Temp.dat
\end{verbatim}

<<load>>=
require(ggplot2)
require(plyr)
fixSizeA <- 10
fixSizeB <- 40
fixSizeC <- 100
fixStride <- 1
#ggsave.latex <- AFLP:::ggsave.latex
line_stat <- function(fun, geom="point", ...) { stat_summary(fun.y=fun, colour="black", geom=geom, size = 1.2, ...) } 
cap_name <- function(num){
  output <- "STRIDE = ";
  output <- paste(output,num,sep="")
  return(output)
}

#Ploting all threads for specific SIZE
plot_size <- function(dataf,stride,size,output="",width=0,height=0) {
  attach(dataf) 
  dataf$NUM <- ceiling(dataf$NUM/(length(unique(STRIDE))*length(unique(SIZE))))  
  dataf <- dataf[STRIDE==stride & SIZE==size,]  

p <- ggplot(data = dataf, aes(NUM, BANDWIDTH)) + aes(shape = factor(THREAD)) + 
  geom_point(aes(colour = factor(THREAD)), size = 2) +
# geom_point(colour="grey90", size = 0.5) +
# geom_line(aes(colour = factor(THREAD))) +
  labs(shape = "THREAD:", colour = "THREAD:") 

p <- p +
     scale_y_continuous("BANDWIDTH (MB/s)") +
     scale_x_continuous("MEASUREMENT")
  
  if(output=="") {
    print(p)
  } else {
    if (width!=0 & height!=0){
      png(output,width,height)
    }
    else{
      png(output)
    }
    print(p)
    garbage <- dev.off()
  }
  detach(dataf)
}
plot_rect <- function(dataf,stride,size,measurement=10,start_limit=0,end_limit=0,output="",width=0,height=0) {
  attach(dataf)  
  dataf$NUM <- ceiling(dataf$NUM/(length(unique(STRIDE))*length(unique(SIZE))))  
  dataf <- dataf[STRIDE==stride & SIZE==size & dataf$NUM==measurement,]
   
  mmm <- min(dataf$START_TIME)
  dataf$START_TIME <- dataf$START_TIME - mmm
  dataf$END_TIME <- dataf$END_TIME - mmm 
  if(end_limit!=0)
    {
      dataf$END_TIME <- pmin(dataf$END_TIME,end_limit)
      dataf$START_TIME <- pmax(dataf$START_TIME,start_limit)
    }

p <- ggplot(data = dataf, aes(xmin = THREAD-0.5, xmax = THREAD + 0.5, ymin = START_TIME, ymax = END_TIME)) +
 # aes(colour = factor(THREAD)) + 
  geom_rect(aes(fill=factor(THREAD))) +
  labs(fill = "THREAD:") 

p <- p + scale_x_continuous("THREAD") + scale_y_continuous("TIME (s)")
if(end_limit!=0)
  p <- p + scale_y_continuous("TIME (s)")+ ylim(start_limit,end_limit)
  
if(output=="") {
    print(p)
  } else {
    if (width!=0 & height!=0){
      png(output,width,height)
    }
    else{
      png(output)
    }
    print(p)
    garbage <- dev.off()
  }
  detach(dataf)
}
plot_Allrect <- function(dataf,minN=1,maxN=42,output="",width=0,height=0) {
  attach(dataf)  
  
  dataf <- dataf[NUM>=minN & NUM<=maxN,]
    
  for(i in min(NUM):max(NUM))
    {
      mmm <- min(dataf[dataf$NUM==i,"START_TIME"])
      for(j in min(THREAD):max(THREAD))
        {
          dataf[dataf$NUM==i & dataf$THREAD==j,"START_TIME"] <- dataf[dataf$NUM==i & dataf$THREAD==j,"START_TIME"] - mmm
          dataf[dataf$NUM==i & dataf$THREAD==j,"END_TIME"] <- dataf[dataf$NUM==i & dataf$THREAD==j,"END_TIME"] - mmm 
        }
    }

p <- ggplot(data = dataf, aes(xmin = THREAD-0.5, xmax = THREAD + 0.5, ymin = START_TIME, ymax = END_TIME)) +
  geom_rect(aes(fill=factor(THREAD))) +
  labs(fill = "THREAD:") 

p <- p +
     scale_y_continuous("TIME (s)") +
     scale_x_continuous("THREAD") +
     facet_wrap(~ NUM, ncol = floor(sqrt(maxN-minN)))
  
  if(output=="") {
    print(p)
  } else {
    if (width!=0 & height!=0){
      png(output,width,height)
    }
    else{
      png(output)
    }
    print(p)
    garbage <- dev.off()
  }
  detach(dataf)
}
pick_size<- function(dataf,stride,size) {
  attach(dataf)  
  NN <- nrow(dataf)/(length(unique(STRIDE))*length(unique(SIZE)))
  dataf <- dataf[STRIDE==stride & SIZE==size,]
  dataf$NUM <- c(1:NN)
  dataf$NUM <- ceiling(dataf$NUM/4)
  detach(dataf)
  return(dataf)
}
#Execution priority
con <- file("TempParameters.dat", "rt")
line <- scan(con,"",20)
prmode <- line[13]
prnum <- line[14]
prmode <- if(prmode=="normal") "NORMAL" else
          if(prmode=="chrt")   "CHRT" else
          if(prmode=="nice")   "NICE" else "/"

read_file <- function(file) {
  dataf <- read.table(file, header=T)
  dataf$SIZE <- dataf$SIZE/1024
  dataf$PRIORITY <- apply(dataf,1,function(row) prmode)
  return(dataf)
}

L1cache <- 32
L2cache <- 256
fixY <- FALSE
fixYMIN <- 600
fixYMAX <- 1300
@

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage 
%Plot from file Temp.dat:
<<label=threads0,echo=FALSE,include=FALSE>>=
dataf <- read_file('Temp.dat')
attach(dataf) 
Nthreads <- length(unique(THREAD))
Ncores <- length(unique(CORE))

p <- ggplot(data = dataf[STRIDE==fixStride,], aes(SIZE, BANDWIDTH)) +  geom_point(colour = "red", alpha=.3) 
p <- p + scale_y_continuous("BANDWIDTH (MB/s)") 
p <- p + scale_x_continuous("MEMORY SIZE (KB)") 

png('threads0.png')
print(p)
garbage <- dev.off()  
detach(dataf)
@

\begin{figure}[h]
\begin{center}  
  \scalebox{0.8}{
  \includegraphics{threads0.png}
  }
\end{center}
\caption{\Sexpr{Nthreads} threads, running on \Sexpr{Ncores} different cores; STRIDE=\Sexpr{fixStride} }
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage 
%Plot from file Temp.dat:
<<label=threads1,echo=FALSE,include=FALSE>>=
dataf <- read_file('Temp.dat')
plot_size(dataf=dataf,stride=fixStride,size=fixSizeB,output='threads.png')
@

\begin{figure}[h]
\begin{center}  
  \scalebox{0.8}{
  \includegraphics{threads.png}
  }
\end{center}
\caption{For the buffer SIZE=\Sexpr{fixSizeB}KB}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage 
%Plot from file Temp.dat:
<<label=threads2,echo=FALSE,include=FALSE>>=
fixSize <- 10
dataf <- read_file('Temp.dat')
measurement <- 10
plot_rect(dataf=dataf,stride=fixStride,size=fixSizeB,measurement=measurement,output='threads2.png',width=400,height=150)
plot_rect(dataf=dataf,stride=fixStride,size=fixSizeB,measurement=measurement,start_limit=0,end_limit=0.00001,output='threads2b.png',width=400,height=150)
plot_rect(dataf=dataf,stride=fixStride,size=fixSizeB,measurement=measurement,start_limit=0.00216,end_limit=0.00217,output='threads2c.png',width=400,height=150)
@

\begin{figure}
  \begin{center}
    \subfloat[All]{\includegraphics{threads2.png}} \\
    \subfloat[Beginning]{\includegraphics{threads2b.png}}\hspace{1em}
    \subfloat[Context switch]{\includegraphics{threads2c.png}}
  \end{center}
%  \caption{Caption}
  \caption{For the buffer SIZE=\Sexpr{fixSizeB}KB; measurement=\Sexpr{measurement}}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage 
%Plot from file Temp.dat:
<<label=threads3,echo=FALSE,include=FALSE>>=
dataf <- read_file('Temp.dat')
plot_Allrect(dataf=pick_size(dataf,fixStride,fixSizeA),output='threads3.png')
plot_Allrect(dataf=pick_size(dataf,fixStride,fixSizeB),output='threads4.png')
plot_Allrect(dataf=pick_size(dataf,fixStride,fixSizeC),output='threads5.png')
@

\begin{figure}[h]
\begin{center}  
  \scalebox{0.8}{
  \includegraphics{threads3.png}
  }
\end{center}
\caption{For the buffer SIZE=\Sexpr{fixSizeA}KB}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage 

\begin{figure}[h]
\begin{center}  
  \scalebox{0.8}{
  \includegraphics{threads4.png}
  }
\end{center}
\caption{For the buffer SIZE=\Sexpr{fixSizeB}KB}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage 

\begin{figure}[h]
\begin{center}  
  \scalebox{0.8}{
  \includegraphics{threads5.png}
  }
\end{center}
\caption{For the buffer SIZE=\Sexpr{fixSizeC}KB}
\end{figure}

\end{document}
