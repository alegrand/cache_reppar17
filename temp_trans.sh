#!/bin/bash
# Script for transfering data files to temporary files for analysis

first=28
last=35
j=1

while [ $first -le $last ] ; do

    grep -v '#' dataPawUBuff3/ParaKData${first}.dat | sed -e 's/  */ /g' -e 's/://g' | cut -d ' ' -f 2,3,4,5,6,10,11 > analysis/TempComp${j}.dat
    first=`expr $first + 1`
    j=`expr $j + 1`

done
