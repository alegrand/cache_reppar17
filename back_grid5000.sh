#!/bin/bash
# Script for copying data from GRID5000 access machine to my machine 

echo "Copying data from grid5000 machine to my machine"
#Producing the original name for data folder
bkup=1
while [ -e dataG5000G${bkup}* ] ; do 
  bkup=`expr $bkup + 1`
done
scp -r lustanisic@access.grenoble.grid5000.fr:ParaK/data dataG5000G${bkup}
