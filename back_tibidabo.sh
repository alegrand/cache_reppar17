#!/bin/bash
# Script for copying data from tibidabo machine to my machine 

echo "Copying data from tibidabo machine to my machine"
#Producing the original name for data folder
bkup=1
while [ -e dataTib${bkup} ] ; do 
  bkup=`expr $bkup + 1`
done
scp -r alegrand@tibidabo.bsc.es:ParaK/data dataTib${bkup}

