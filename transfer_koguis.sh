#!/bin/bash
# Script for copying ParaK to koguis machine

echo "Copying ParaK to koguis"
mkdir Empty
scp transfer_snow.sh lstanisic@koguis.imag.fr:transfer_snow.sh
scp -r Empty lstanisic@koguis.imag.fr:ParaK
scp -r Empty lstanisic@koguis.imag.fr:ParaK/data
scp -r src lstanisic@koguis.imag.fr:ParaK/src
scp run_ARM.sh lstanisic@koguis.imag.fr:ParaK/run.sh
scp src/Makefile_ARM lstanisic@koguis.imag.fr:ParaK/src/Makefile
scp Parameters.txt lstanisic@koguis.imag.fr:ParaK
scp back_snow.sh lstanisic@koguis.imag.fr:ParaK
rmdir Empty

ssh lstanisic@koguis.imag.fr
